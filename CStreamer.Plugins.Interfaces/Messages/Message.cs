﻿// -----------------------------------------------------------------------
// <copyright file="Message.cs" company="LuckySkebe (fmann12345@gmail.com)">
//     Copyright (c) LuckySkebe (fmann12345@gmail.com). All rights reserved.
//     Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>
// -----------------------------------------------------------------------

namespace CStreamer.Plugins.Interfaces.Messages
{
    /// <summary>
    /// An abstract base class to all messages that can be sent to a Bin / Pipeline.
    /// </summary>
    public abstract class Message
    {
    }
}